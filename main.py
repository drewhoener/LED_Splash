import random
import time
from threading import Event

from rpi_ws281x import *

# LED strip configuration:
from threads import LEDThread, FileThread

LED_COUNT = 300  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


def color_fade(strip, wait_ms=20, iterations=5):
    for i in range(iterations):
        for j in range(256):
            for k in range(strip.numPixels()):
                strip.setPixelColor(k, Color(j, 0, 0))
            strip.show()
            time.sleep(wait_ms / 1000)
        for j in range(256):
            for k in range(strip.numPixels()):
                strip.setPixelColor(k, Color(255 - j, 0, 0))
            strip.show()
            time.sleep(wait_ms / 1000)


def twinkle_rainbow(strip, wait_ms=10, iterations=5, percent_lit = 20):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, 0, 0, 0)
    strip.show()
    stack = []
    for i in range(256 * iterations):
        pixel = random.randrange(0, strip.numPixels())
        if pixel in stack:
            continue
        stack.append(pixel)
        strip.setPixelColor(pixel, wheel(i & 255))
        if len(stack) > int(strip.numPixels() * (percent_lit / 100)):
            strip.setPixelColor(stack.pop(0), Color(0, 0, 0))
        strip.show()
        time.sleep(wait_ms / 1000)


if __name__ == '__main__':
    signal_event = Event()
    signal_event.set()

    led_thread = LEDThread(signal_event, 1, "LED Thread")
    file_thread = FileThread(signal_event, 2, "File Thread", led_thread)

    led_thread.start()
    file_thread.start()

    try:
        while True:
            time.sleep(.5)
    except KeyboardInterrupt:
        print("Received Interrupt, cleaning up threads")
        signal_event.clear()
        led_thread.join(10)
        file_thread.join(10)
        print("All Threads closed")
