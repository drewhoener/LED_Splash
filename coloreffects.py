import random
from time import sleep

from rpi_ws281x import *

from colorpacket import ColorMode


def clamp(n, smallest, largest): return max(smallest, min(n, largest))


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


class ColorEffect:

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        print("Init Effect")
        self.id = -1
        self.running = True
        self.brightness_queue = []
        self.color = color
        self.rainbow = rainbow
        self.min_brightness = max(0, min_brightness)
        self.max_brightness = min(255, max_brightness)
        self.delay = delay
        self.decay = decay

    def setAll(self, strip, color, delay=10):

        for i in range(strip.numPixels()):
            strip.setPixelColor(i, color)
            if delay > 0:
                sleep(delay / 1000.0)

    def scaleColor(self, color, scale):
        red = min(0xFF, scale * ((color & 0xFF0000) >> 16))
        green = min(0xFF, scale * ((color & 0x00FF00) >> 8))
        blue = min(0xFF, scale * (color & 0xFF))

        return Color(int(red), int(green), int(blue))

    def reset(self):
        self.running = True
        self.color = 0
        self.rainbow = 0
        self.min_brightness = 7
        self.max_brightness = 255
        self.delay = 10

    def modify(self, color_packet):
        self.color = color_packet.color
        self.rainbow = color_packet.rainbow_mode
        self.decay = color_packet.decay
        self.min_brightness = color_packet.min_scale
        self.max_brightness = color_packet.max_scale
        self.delay = color_packet.delay_ms

    def setup(self, strip):
        print("Setting up LED Strip")
        self.setAll(strip, 0x000000, 0)
        strip.show()

    """
    Defines a single run of this effect
    """

    def run(self, strip):
        pass


class PulseEffect(ColorEffect):
    """
    Rainbow Mode:
        0: Off
        1: Constant up-down cycle.
    """

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        super(PulseEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = ColorMode.PULSE.value
        self.offset = 127

    def run(self, strip):
        for i in range(self.min_brightness, ((self.max_brightness + 1) * 2) - self.min_brightness):
            brightness = i if i <= self.max_brightness else self.max_brightness - (i - self.max_brightness)
            # print("Brightness is %d" % brightness)
            scale = brightness / 255
            for pixel in range(strip.numPixels()):
                if not self.running:
                    return
                pixel_color = self.scaleColor(self.color, scale) if not self.rainbow else self.scaleColor(
                    wheel((brightness + self.offset) & 255), scale)
                strip.setPixelColor(pixel, pixel_color)
            strip.show()
            sleep(self.delay / 1000.0)
        self.offset += 63
        if self.offset > 511:
            self.offset = 31


class WipeEffect(ColorEffect):

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        super(WipeEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = ColorMode.WIPE.value
        self.offset = 0

    def run(self, strip):
        if self.rainbow:
            for mod in range(256):
                for pixel in range(strip.numPixels()):
                    strip.setPixelColor(pixel,
                                        wheel((int(pixel * 256 / strip.numPixels()) + (mod + self.offset)) & 255))
                    if not self.running:
                        return
                strip.show()
                sleep(self.delay / 1000.0)
            self.offset += 256
        else:
            for pixel in range(strip.numPixels()):
                if not self.running:
                    return
                strip.setPixelColor(pixel, self.color)
                strip.show()
                sleep(self.delay / 1000.0)
            for pixel in range(strip.numPixels()):
                if not self.running:
                    return
                strip.setPixelColor(pixel, 0)
                strip.show()
                sleep(self.delay / 1000.0)


class ChaseEffect(ColorEffect):

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        super(ChaseEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = ColorMode.CHASE.value

    # TODO add in interrupt
    def run(self, strip):
        for color_mod in range(256 if self.rainbow else 1):
            for order in range(3):
                for pixel in range(0, strip.numPixels(), 3):
                    if not self.running:
                        return
                    color = wheel((pixel + color_mod) % 255) if self.rainbow else self.color
                    strip.setPixelColor(pixel + order, color)
                strip.show()
                sleep(self.delay / 1000.0)
                for pixel in range(0, strip.numPixels(), 3):
                    if not self.running:
                        return
                    strip.setPixelColor(pixel + order, 0)


class TwinkleEffect(ColorEffect):

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, percent_lit=53, decay=20):
        super(TwinkleEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = ColorMode.TWINKLE.value
        self.stack = []
        self.percent_lit = percent_lit

    def reset(self):
        super(TwinkleEffect, self).reset()
        self.percent_lit = 53

    def setup(self, strip):
        super(TwinkleEffect, self).setup(strip)
        self.stack = []

    def modify(self, color_packet):
        super(TwinkleEffect, self).modify(color_packet)
        self.percent_lit = color_packet.twinkle_ratio

    # TODO add in interrupt
    def run(self, strip):
        for i in range(256):
            if not self.running:
                return
            pixel = random.randrange(0, strip.numPixels())
            if pixel in self.stack:
                continue
            self.stack.append(pixel)
            pixel_color = wheel(i & 255) if self.rainbow else self.color
            strip.setPixelColor(pixel, pixel_color)
            while len(self.stack) > int(strip.numPixels() * (self.percent_lit / 100)):
                strip.setPixelColor(self.stack.pop(0), Color(0, 0, 0))
            strip.show()
            sleep(self.delay / 1000)


class SolidEffect(ColorEffect):

    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        super(SolidEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = ColorMode.SOLID.value
        self.brightness_flag = True
        self.color_flag = True

    def modify(self, color_packet):
        old_color = self.color
        old_brightness = self.max_brightness
        super().modify(color_packet)
        if old_color != self.color:
            self.color_flag = True
        if old_brightness != self.max_brightness:
            self.brightness_flag = True

    def run(self, strip):
        if self.rainbow:
            for pixel_color in range(256):
                for pixel in range(strip.numPixels()):
                    if not self.running:
                        return
                    strip.setPixelColor(pixel, wheel(pixel_color))
                strip.show()
        else:
            if self.color == -1:
                print("Invalid color")
                return
            if self.brightness_flag:
                print("Brightness flag, cur color is %s" % self.color)
                self.color = self.scaleColor(self.color, float(self.max_brightness) / 255.0)
                print("Color scaled to %s" % self.color)
                self.brightness_flag = False
                self.color_flag = True
            if self.color_flag:
                # print("Cur Color: %d, Set Color:%d" % (self.cur_color, self.color))
                for pixel in range(strip.numPixels()):
                    if not self.running:
                        print("Solid effect no longer running")
                        return
                    # print("Setting color of %d to %s" % (pixel, self.color))
                    strip.setPixelColor(pixel, self.color)
                self.color_flag = False
                strip.show()


class OffEffect(ColorEffect):
    def __init__(self, color=0, rainbow=0, min_brightness=7, max_brightness=255, delay=10, decay=20):
        super(OffEffect, self).__init__(color, rainbow, min_brightness, max_brightness, delay, decay)
        self.id = 0
        self.turn_off = True

    def modify(self, color_packet):
        super(OffEffect, self).modify(color_packet)
        self.turn_off = True

    def run(self, strip):
        if self.turn_off:
            self.setAll(strip, 0, 0)
            strip.show()
            self.turn_off = False


effect_dict = {
    ColorMode.OFF.value: OffEffect(),
    ColorMode.PULSE.value: PulseEffect(),
    ColorMode.WIPE.value: WipeEffect(),
    ColorMode.CHASE.value: ChaseEffect(),
    ColorMode.TWINKLE.value: TwinkleEffect(),
    ColorMode.SOLID.value: SolidEffect()
}


def get_effect(packet_id):
    return effect_dict.get(packet_id, effect_dict[ColorMode.PULSE.value])
