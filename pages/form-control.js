(function () {
    function toJSONString(form) {
        var obj = {};
        var elements = form.querySelectorAll("input, select, textarea");
        for (var i = 0; i < elements.length; ++i) {
            var element = elements[i];
            var name = element.name;
            var value = element.value;

            if (name) {
                obj[name] = value;
            }
        }

        var red = obj['red'];
        var green = obj['green'];
        var blue = obj['blue'];

        obj['color'] = (red << 16) | (green << 8) | blue;
        obj['__type__'] = "ColorPacket";

        return JSON.stringify(obj);
    }

    document.addEventListener("DOMContentLoaded", function () {
        var form = document.getElementById("test");
        var output = document.getElementById("output");
        form.addEventListener("submit", function (e) {
            e.preventDefault();
            var json = toJSONString(this);
            console.log(json);
            output.innerHTML = json;

            var data = new FormData();
            data.append("data", json);
            var xhr = new XMLHttpRequest();
            xhr.open('post', './save.php', true);
            xhr.send(data);
        }, false);

        var red = 127;
        var green = 127;
        var blue = 127;

        var RGBChange = function () {
            $('#rgbpanel').css('background', 'rgb(' + red + ',' + green + ',' + blue + ')')
        };

        var r = $('#slider-red').ionRangeSlider({
            min: 0,
            max: 255,
            from: 127,
            onChange: function (data) {
                red = data.from;
                RGBChange();
            }
        }).data('ionRangeSlider');

        var g = $('#slider-green').ionRangeSlider({
            min: 0,
            max: 255,
            from: 127,
            onChange: function (data) {
                green = data.from;
                RGBChange();
            }
        }).data('ionRangeSlider');

        var b = $('#slider-blue').ionRangeSlider({
            min: 0,
            max: 255,
            from: 127,
            onChange: function (data) {
                blue = data.from;
                RGBChange();
            }
        }).data('ionRangeSlider');

        var delay = $('#slider-delay').ionRangeSlider({
            min: 0,
            max: 500,
            from: 20
        }).data('ionRangeSlider');

        var delay = $('#slider-twinkle').ionRangeSlider({
            min: 0,
            max: 100,
            from: 25
        }).data('ionRangeSlider');

        var delay = $('#slider-decay').ionRangeSlider({
            min: 0,
            max: 100,
            from: 20
        }).data('ionRangeSlider');
    });

})();