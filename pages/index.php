<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>Lighting Control</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="./ion.rangeSlider-2.2.0/css/ion.rangeSlider.css">
  <link rel="stylesheet" href="./ion.rangeSlider-2.2.0/css/normalize.css">
  <link rel="stylesheet" href="./ion.rangeSlider-2.2.0/css/ion.rangeSlider.skinFlat.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="./ion.rangeSlider-2.2.0/js/ion.rangeSlider.min.js"></script>
  <script src="./scripts/form-control.js"></script>
  <style type="text/css">
    body {
      margin: 2em auto;
      max-width: 75%;
    }

    form div {
      margin-bottom: 0.5em;
    }

    form div label,
    form div input {
      //display: block;
      margin-bottom: 0.3em;
    }
  </style>
</head>

<body>
  <div class="container">
    <form id="test" action="#" method="post">
      <div class="form-group">
        <label for="color_mode">Color Mode</label>
        <select name="color_mode" class="form-control">
      		  <option value=1 selected="selected">Pulse</option>
      		  <option value=2>Wipe</option>
      		  <option value=3>Chase</option>
      		  <option value=4>Twinkle</option>
              <option value=5>Solid</option>
              <option value=0>Off</option>
      	</select>
      </div>
      <div class="card">
        <div class="card-header">
          <h3 class="panel-title">Rainbow & Brightness</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-md-4">
              <label for="rainbow_mode">Rainbow</label>
              <select name="rainbow_mode" class="form-control">
              <option value=0 selected="selected">Off</option>
              <option value=1>On</option>
            </select>
            </div>
            <div class="form-group col-md-4">
              <label for="min_scale">Min Brightness</label>
              <input class="form-control" type="number" name="min_scale" , value="5" , min="0" , max="255" />
            </div>
            <div class="form-group col-md-4">
              <label for="max_scale">Max Brightness</label>
              <input class="form-control" type="number" name="max_scale" , value="255" , min="0" , max="255" />
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div id="rgbpanel" class="card-header">
          <h3 class="panel-title">Color</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-md-4">
              <label for="red">Red  </label>
              <input type="text" id="slider-red" name="red" value="" />
            </div>
            <div class="form-group col-md-4">
              <label for="green">Green  </label>
              <input type="text" id="slider-green" name="green" value="" />
            </div>
            <div class="form-group col-md-4">
              <label for="blue">Blue  </label>
              <input type="text" id="slider-blue" name="blue" value="" />
            </div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          <h3 class="panel-title">Delay & Special</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-md-4">
              <label for="delay">Delay  </label>
              <input type="text" id="slider-delay" name="delay" value="" />
            </div>
            <div class="form-group col-md-4">
              <label for="twinkle_ratio">Twinkle Ratio  </label>
              <input type="text" id="slider-twinkle" name="twinkle_ratio" value="" />
            </div>
            <div class="form-group col-md-4">
              <label for="decay">Decay  </label>
              <input type="text" id="slider-decay" name="decay" value="" />
            </div>
          </div>
        </div>
      </div>
      <p>
        <input type="submit" value="Send" class="btn btn-primary btn-block" />
      </p>
    </form>
  </div>
  <pre id="output"></pre>
</body>

</html>
