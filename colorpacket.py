import json
from enum import Enum


def as_color_packet(packet_dict):
    if '__type__' in packet_dict and packet_dict['__type__'] == 'ColorPacket':
        return ColorPacket(int(packet_dict['color_mode']), int(packet_dict['rainbow_mode']), int(packet_dict['color']),
                           int(packet_dict['decay']), int(packet_dict['twinkle_ratio']),
                           int(packet_dict['min_scale']), int(packet_dict['max_scale']), int(packet_dict['delay']))


def decode_packet(json_data):
    try:
        return json.loads(json_data, object_hook=as_color_packet)
    except ValueError:
        print("Excepted Value: ({0})".format(json_data))
        return None


class ColorPacket:

    def __init__(self, color_mode=0, rainbow_mode=0, color=0, decay=0, twinkle_ratio=53, min_scale=0, max_scale=255,
                 delay_ms=15):
        self.color_mode = color_mode
        self.rainbow_mode = rainbow_mode
        self.decay = decay
        self.twinkle_ratio = twinkle_ratio
        self.color = color
        self.min_scale = min_scale
        self.max_scale = max_scale
        self.delay_ms = delay_ms


class ColorMode(Enum):
    OFF = 0
    PULSE = 1
    WIPE = 2
    CHASE = 3
    TWINKLE = 4
    SOLID = 5
    FLARE = 6
    METEOR = 7

