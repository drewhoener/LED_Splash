import threading
from pathlib import Path

from rpi_ws281x import *

from coloreffects import get_effect
from colorpacket import decode_packet, ColorPacket

# LED strip configuration:
LED_COUNT = 418  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53


class FileThread(threading.Thread):

    def __init__(self, event, thread_id, name, led_thread):
        super(FileThread, self).__init__()
        self.event = event
        self.threadID = thread_id
        self.led_thread = led_thread
        self.name = name
        self.running = True

    def run(self):
        print("Starting Thread {0}".format(self.name))
        packet_file = Path("/var/www/html/cur_packet")
        while self.event.is_set():
            if packet_file.is_file():
                file_stream = open(str(packet_file), "r+")
                for line in file_stream:
                    if not self.running or file_stream.closed:
                        self.running = False
                        break
                    packet = decode_packet(line)
                    if isinstance(packet, ColorPacket):
                        print("Found line {0}".format(line))
                        self.led_thread.new_effect(packet)
                file_stream.truncate(0)
                file_stream.close()

        print("Exiting Thread {0}".format(self.name))


class LEDThread(threading.Thread):

    def __init__(self, event, thread_id, name):
        super(LEDThread, self).__init__()
        self.event = event
        self.threadID = thread_id
        self.name = name
        self.running = True
        self.cur_packet = None
        self.next_packet = None
        self.strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS,
                                       LED_CHANNEL)

    def run(self):
        self.strip.begin()
        print("Starting Thread {0}".format(self.name))
        while self.event.is_set():
            if self.next_packet is not None:
                print("Next packet found")
                if self.cur_packet is not None:
                    print("Resetting cur packet")
                    self.cur_packet.reset()
                print("Setting cur")
                self.cur_packet = self.next_packet
                self.next_packet = None
                print("Setting up cur")
                self.cur_packet.setup(self.strip)
            # print(type(self.cur_packet))
            if self.cur_packet is not None:
                # print("Running")
                self.cur_packet.run(self.strip)
        print("Setting LEDS 0")
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, 0)
        self.strip.show()
        self.cur_packet.running = False
        print("Exiting Thread: {0}".format(self.name))

    def new_effect(self, packet):
        new_packet = get_effect(packet.color_mode)
        print(type(new_packet))
        if self.cur_packet is not None and isinstance(new_packet, type(self.cur_packet)):
            print("Modify packet")
            self.cur_packet.modify(packet)
        else:
            print("Next packet set")
            self.next_packet = new_packet
            print("Next modify")
            self.next_packet.modify(packet)
            if self.cur_packet is not None:
                print("Cur packet running false")
                print(type(self.cur_packet))
                self.cur_packet.running = False
